# Init

```
git clone git@gitee.com:andals/docker-centos.git centos
cd centos
prjHome=`pwd`
centosVer=7
```

# Build pkg

```
docker run -i -t -v $prjHome/:/build --name=build-centos-${centosVer} andals/centos:${centosVer} /bin/bash
/build/script/build_pkg.sh
```

# Build image

```
docker build -t andals/centos:${centosVer} ./
```

# Run data volumn home

```
docker run -d -v /home:/home -v /myspace:/myspace --name data-home andals/centos:${centosVer} echo Data Volumn Home
```
