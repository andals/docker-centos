FROM centos:7.3.1611
MAINTAINER ligang <ligang1109@aliyun.com>

LABEL name="CentOS Image"
LABEL vendor="Andals"

COPY pkg/ /build/pkg/
COPY script/ /build/script/

RUN /build/script/build_image.sh

CMD /build/script/init.sh
